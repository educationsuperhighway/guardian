winston           = require 'winston'
expressWinston    = require 'express-winston'

config            = require './config'

winstonConsole = new winston.transports.Console
  level: config.log.level
  colorize: true
  timestamp: true

winstonFile = new winston.transports.File
  level: config.log.level
  colorize: false
  timestamp: true
  filename: config.log.file
  maxsize: 10 * 1024 * 1024

logger = new winston.Logger(transports: [ winstonConsole, winstonFile ])

logger.configureExpress = (app) ->
  app.use expressWinston.logger
    transports: [ winstonConsole, winstonFile ]
    msg: 'HTTP {{req.method}} {{req.url}}'
  app.use expressWinston.errorLogger
    transports: [ winstonConsole, winstonFile ]

module.exports = logger

