set 'deployTo', 'CHANGEME'
set 'keepReleases', 10
set 'hosts', [ 'CHANGEME' ] if roco.env == 'production'

namespace 'deploy', ->
  task 'start', (done) ->
    run "cd #{roco.currentPath} && forever start -c coffee app.coffee", done
  task 'stop', (done) ->
    run "cd #{roco.currentPath} && forever stop app.coffee", done
  task 'restart', (done) ->
    run """
      if [[ `forever list` =~ 'app.coffee' ]]; then
        cd #{roco.currentPath} && forever stop app.coffee
      fi
      cd #{roco.currentPath} && gulp build && forever start -c coffee app.coffee
    """, done
