gulp        = require 'gulp'
clean       = require 'gulp-clean'
coffee      = require 'gulp-coffee'
concat      = require 'gulp-concat'
uglify      = require 'gulp-uglify'
concat      = require 'gulp-concat'
minifyCSS   = require 'gulp-minify-css'
nodemon     = require 'gulp-nodemon'
gzip        = require 'gulp-gzip'
stylus      = require 'gulp-stylus'
wait        = require 'gulp-wait'

paths =
  css: [
    'vendor/assets/stylesheets/*.css'
    'app/assets/stylesheets/*.css'
  ]
  js: [
    'vendor/assets/javascripts/*.js'
    'app/assets/javascripts/*.js'
  ]
  stylus: [
    'vendor/assets/stylesheets/*.styl'
    'app/assets/stylesheets/*.styl'
  ]
  coffee: [
    'vendor/assets/javascripts/*.coffee'
    'app/assets/javascripts/*.coffee'
  ]
  images: [
    'vendor/assets/images/*'
    'app/assets/images/*'
  ]

includes = [
  'jquery-1.11.1.js'
  'tree.jquery.js'
  'bootstrap.js'
  'application.js'
]

gulp.task 'clean-build', ->
  gulp.src 'build/**/*', read: false
    .pipe clean()

gulp.task 'clean-public', ->
  gulp.src 'public/**/*', read: false
    .pipe clean()

doCss = ->
  gulp.src paths.css
    .pipe gulp.dest 'build/stylesheets'

gulp.task 'css', [ 'clean-build' ], doCss
gulp.task 'watch-css', doCss

doJs = ->
  gulp.src paths.js
    .pipe gulp.dest 'build/javascripts'

gulp.task 'js', [ 'clean-build' ], doJs
gulp.task 'watch-js', doJs

doStylus = ->
  gulp.src paths.stylus
    .pipe stylus()
    .pipe gulp.dest 'build/stylesheets'

gulp.task 'stylus', [ 'clean-build' ], doStylus
gulp.task 'watch-stylus', doStylus

doCoffee = ->
  gulp.src paths.coffee
    .pipe coffee bare: true
    .pipe gulp.dest 'build/javascripts'

gulp.task 'coffee', [ 'clean-build' ], doCoffee
gulp.task 'watch-coffee', doCoffee

doStyles = ->
  gulp.src [ 'build/stylesheets/**/*.css' ]
    #.pipe minifyCSS()
    .pipe concat 'all.min.css'
    .pipe gzip()
    .pipe gulp.dest 'public/stylesheets'

gulp.task 'styles', [ 'clean-public', 'css', 'stylus' ], doStyles
gulp.task 'watch-styles', [ 'watch-css', 'watch-stylus' ], doStyles

doScripts = ->
  files = []
  includes.forEach (file) -> files.push 'build/javascripts/' + file
  gulp.src files
    #.pipe uglify()
    .pipe concat 'all.min.js'
    .pipe gzip()
    .pipe gulp.dest 'public/javascripts'

gulp.task 'scripts', [ 'clean-public', 'js', 'coffee' ], doScripts
gulp.task 'watch-scripts', [ 'watch-js', 'watch-coffee' ], doScripts

gulp.task 'images', [ 'clean-public' ], ->
  gulp.src paths.images
    .pipe gulp.dest 'public/images'

gulp.task 'run', ->
  nodemon script: 'app.coffee', ext: 'coffee', ignore: [ 'public/**' ]

gulp.task 'watch', ->
  gulp.watch paths.css,       (e) -> gulp.run 'watch-styles'
  gulp.watch paths.stylus,    (e) -> gulp.run 'watch-styles'
  gulp.watch paths.js,        (e) -> gulp.run 'watch-scripts'
  gulp.watch paths.coffee,    (e) -> gulp.run 'watch-scripts'

gulp.task 'build', [ 'styles', 'scripts', 'images' ]
gulp.task 'default', [ 'styles', 'scripts', 'images', 'watch', 'run' ]

