
randInt = (min, max) -> Math.floor(Math.random() * (max - min + 1)) + min

exports.uid = (len) ->
  buf = []
  chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  charlen = chars.length
  buf.push(chars[randInt(0, charlen - 1)]) for i in [ 0 .. charlen - 1]
  buf.join ''

