mongoose    = require 'mongoose'
oauthorize  = require 'oauthorize'

logger      = require './logger'
Client      = mongoose.model 'Client'

oauth = oauthorize.createServer()

# For storing and retrieving oauth data from the session cookie

oauth.serializeClient (client, done) ->
  logger.debug 'serializeClient by id ' + client.id
  done null, client.id

oauth.deserializeClient (id, done) ->
  logger.debug 'deserializeClient by id ' + id
  Client.findById id, (err, client) ->
    done err, client

module.exports = oauth

