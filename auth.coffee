mongoose            = require 'mongoose'
passport            = require 'passport'
LocalStrategy       = require('passport-local').Strategy
{ConsumerStrategy}  = require 'passport-http-oauth'
{TokenStrategy}     = require 'passport-http-oauth'

logger              = require './logger'
User                = mongoose.model 'User'
Client              = mongoose.model 'Client'
RequestToken        = mongoose.model 'RequestToken'

passport.use new LocalStrategy
  usernameField: 'user[email]'
  passwordField: 'user[password]'
, (email, password, done) ->
  User.findOne email: email, (err, user) ->
    return done err if err?
    return done null, false, alert: 'User not found.' unless user?
    return done null, false, alert: 'Invalid login credentials.' unless user.password == password
    done null, user

passport.serializeUser (user, done) ->
  logger.debug 'serializeUser by id ' + user.id
  done null, user.id

passport.deserializeUser (id, done) ->
  logger.debug 'deserializeUser by id ' + id
  User.findById id, (err, user) ->
    done err, user

passport.use 'consumer', new ConsumerStrategy \
  (key, done) ->
    Client.findOne key: key, (err, client) ->
      return done err if err?
      unless client?
        return done new Error 'Could not find client by key ' + key
      done null, client, client.secret
, (key, done) ->
    RequestToken.findOne key: key, (err, rt) ->
      return done err if err?
      return done new Error 'Could not find request token with key ' + key unless rt?
      done null, rt.secret, rt
, (timestamp, nonce, done) ->
    done null, true

passport.use 'token', new TokenStrategy \
  (key, done) ->
    Client.findOne key: key, (err, client) ->
      return done err if err?
      unless client?
        return done new Error 'Could not find client by key ' + key
      done err, client, client.secret
, (key, done) ->
    AccessToken.findOne key: key, (err, at) ->
      if err?
        return done err
      unless at?
        return done new Error 'Could not find access token by key ' + key
      User.findById at.userId, (err, user) ->
        if err?
          return done err
        unless user?
          return done new Error 'Could not find user by key ' + key
        done null, user, at.secret, at
