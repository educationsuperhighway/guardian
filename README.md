
# Guardian

Guardian is a prototype we did for an OAuth single sign on server for EducationSuperhighway. Ultimately we went with another solution, but this prototype might be useful for you if you want to build a single sign-on solution in Node. It relies on the "OAuthorize" and "Passport" libs.

## Development

```
$ npm install -g forever gulp coffee-script
# In repository root
$ npm install
$ gulp
```

## Installation

### Configuration

You'll want to grep the repository for "CHANGEME" which indicates where a configuration is necessary.

### Dependencies

On Ubuntu (should be mostly translatable to other distros)
```
$ sudo apt-get install git
$ sudo apt-get install mongodb
$ sudo apt-get install nodejs
# If your executable is called "nodejs" instead of "node"
$ sudo ln -s /usr/bin/nodejs /usr/bin/node
$ sudo apt-get install npm
$ mkdir /home/esh/npm-packages
$ echo "prefix = /home/YOURUSER/.npm-packages" > /home/YOURUSER/.npmrc
$ echo "export PATH=$PATH:/home/esh/.npm-packages/bin" >> /home/esh/.bashrc
# Some bashrc's will include logic to skip sourcing when non-interactive.
# You'll have to remove this logic to get Roco automatic deployment to work properly.
$ npm install -g forever gulp coffee-script
# Make sure NODE_ENV is defined
$ echo "export NODE_ENV=production" >> ~/.bashrc
```

### Deployment 

```
# Create the directory structure for deployment
$ roco production deploy:setup
# You must establish the repository as a known host before running this.
# The simplest way to do this is ssh onto the box and do a git clone manually.
$ roco production deploy
```

