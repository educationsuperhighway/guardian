# messages.en.coffee
#
# Example of a messages file for locale 'en'.
#
#
module.exports =
  errors:
    '*':
      '*':
        required: "%(path)s is a required field."
        min: "%(path)s must be greater than %(min)s"
        max: "%(path)s must be less than %(max)s"
        enum: "%(path)s must be included in %(enum)s"

