module.exports =
  environment: 'production'
  app:
    baseUrl: 'CHANGEME'
  db:
    host: 'localhost'
    port: 27017
    name: 'guardian'
  log:
    file: './log/guardian.log'
    level: 'debug'
  mailer:
    from: 'CHANGEME'
    host: 'CHANGEME'
    port: 587
    transportMethod: 'smtp'
    auth:
      user: 'CHANGEME'
      pass: 'CHANGEME'
