module.exports =
  environment: 'development'
  app:
    baseUrl: 'http://localhost:3000'
  db:
    host: 'localhost'
    port: 27017
    name: 'guardian'
  log:
    file: './log/guardian.log'
    level: 'debug'
  mailer:
    from: 'CHANGEME'
    host: 'smtp.mandrillapp.com'
    port: 587
    transportMethod: 'smtp'
    auth:
      user: 'CHANGEME'
      pass: 'CHANGEME'
