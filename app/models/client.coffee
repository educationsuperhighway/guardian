mongoose = require 'mongoose'

schema = new mongoose.Schema
  name:
    type: String
    required: true
  email:
    type: String
    required: true
  key:
    type: String
    required: true
    index:
      unique: true
  secret:
    type: String
    required: true

mongoose.model 'Client', schema
