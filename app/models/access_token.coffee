mongoose = require 'mongoose'

schema = new mongoose.Schema
  clientId:
    type: mongoose.Schema.Types.ObjectId
    ref: 'Client'
    required: true
    index: true
  userId:
    type: mongoose.Schema.Types.ObjectId
    ref: 'User'
    index: true
  key:
    type: String
    required: true
  secret:
    type: String
    required: true
  verifier:
    type: String

mongoose.model 'AccessToken', schema
