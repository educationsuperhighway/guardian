mongoose = require 'mongoose'

schema = new mongoose.Schema
  email:
    type: String
    required: true
    index:
      unique: true
  password:
    type: String
    required: true
  status:
    type: String
    enum: [ 'unverified', 'active', 'locked' ]
  verificationCode:
    type: String

mongoose.model 'User', schema
