login            = require 'connect-ensure-login'
mongoose         = require 'mongoose'
passport         = require 'passport'

oauth            = require '../../oauth'
{uid}            = require '../../utils'
logger           = require '../../logger'

AccessToken      = mongoose.model 'AccessToken'
Client           = mongoose.model 'Client'
RequestToken     = mongoose.model 'RequestToken'
User             = mongoose.model 'User'

class OAuthController

  constructor: (@app) ->
    @app.post '/oauth/request_token', @requestToken
    @app.get '/oauth/authorize', @authorize
    @app.get '/oauth/authorized', @authorized
    @app.post '/oauth/access_token', @accessToken

  # Dispense request token
  requestToken: [
    passport.authenticate 'consumer', session: false
    oauth.requestToken (client, callbackUrl, next) ->
      rt = new RequestToken
        clientId: client.id
        callbackUrl: callbackUrl
        key: uid 8
        secret: uid 32
      rt.save (err) ->
        return next err if err?
        next null, rt.key, rt.secret
  ]

  # Show user authorization
  authorize: [
    login.ensureLoggedIn '/user/login'
    oauth.userAuthorization (key, done) ->
      RequestToken.findOne key: key, (err, rt) ->
        return done err if err?
        return done new Error 'Could not find request token by key ' + key unless rt?
        Client.findById rt.clientId, (err, client) ->
          return done err if err?
          return done new Error 'Could not find client by id ' + rt.clientId unless client?
          done null, client, rt.callbackUrl
    (req, res, next) ->
      req.session.oauth = req.oauth
      res.redirect '/oauth/authorized'
  ]

  # Process user authorization
  authorized: [
    login.ensureLoggedIn '/user/login'
    # Get the oauth transaction out of the session
    (req, res, next) ->
      req.oauth = req.session.oauth
      next()
    oauth.userDecision (key, user, res, next) ->
      verifier = uid 8
      RequestToken.findOne key: key, (err, rt) ->
        return next err if err?
        rt.userId = user.id
        rt.verifier = verifier
        rt.approved = true
        rt.save (err) ->
          return next err if err?
          next null, verifier
  ]

  # Dispense access token
  accessToken: [
    passport.authenticate 'consumer', session: false
    oauth.accessToken \
      # Verify
      (key, verifier, info, next) ->
        return next null, false unless verifier == info.verifier
        next null, true
      # Issue
    , (client, key, info, next) ->
        return next null, false unless info.approved
        return next null, false unless info.clientId.equals client.id
        at = new AccessToken
          clientId: info.clientId
          userId: info.userId
          key: uid 16
          secret: uid 64
        at.save (err) ->
          return next err if err?
          User.findById info.userId, (err, user) ->
            return next err if err?
            return next new Error 'Could not find user by id ' + at.userId unless user?
            next null, at.key, at.secret, email: user.email
  ]

module.exports = OAuthController
