mongoose = require 'mongoose'

class ApplicationController

  constructor: (@app) ->
    @app.get '/', @index

  index: (req, res, next) ->
    res.redirect '/user/login'

module.exports = ApplicationController
