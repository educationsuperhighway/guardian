fml        = require 'form-model-validator'
mongoose   = require 'mongoose'
passport   = require 'passport'

config     = require '../../config'
logger     = require '../../logger'
utils      = require '../../utils'

User       = mongoose.model 'User'

class UserController

  constructor: (@app) ->
    @app.get '/user/create', @create
    @app.post '/user/create', @created
    @app.get '/user/created', @created
    @app.get '/user/verify', @verify
    @app.get '/user/login', @login
    @app.post '/user/login', @authenticate

  create: (req, res, next) ->
    res.render 'user/create'

  created: (req, res, next) =>
    User.findOne email: req.body.user.email, (err, dup) =>
      return next err if err?
      return res.render 'user/create', errors: [ 'An account with that email address already exists.' ] if dup?
      user = new User
      user.email = req.body.user.email
      user.password = req.body.user.password
      user.status = 'unverified'
      user.verificationCode = utils.uid(16)
      user.save (err) =>
        if fml.invalid user
          fml.translate user
          res.render 'user/create', user: user
          return
        return next err if err?
        logger.info 'User ' + user.email + ' was created'
        @app.mailer.send 'user/email/verification',
          user: user
          config: config
          to: user.email
          subject: 'Account Verification'
        , (err) ->
          return next err if err?
          res.render 'user/created', user

  verify: (req, res, next) ->
    User.findOne email: req.query.email, (err, user) ->
      if err?
        next err
        return
      if not user? \
          or user.status != 'unverified' \
          or not req.query.verificationCode?
        res.render '400'
        return
      if req.query.verificationCode != user.verificationCode
        res.render 'user/verify', alert: 'Your verification code does not match.'
        return
      user.status = 'active'
      user.save (err) ->
        return next err if err?
        res.render 'user/verify', notices: [ 'Your account has been successfully verified.' ]

  login: (req, res, next) -> res.render 'user/login'

  authenticate: (req, res, next) ->
    authenticator = passport.authenticate 'local', (err, user, info) ->
      return next err if err?
      return res.render 'user/login', errors: [ 'Incorrect credentials.' ] unless user
      req.logIn user, (err) ->
        return next err if err?
        if req.session.returnTo?
          url = req.session.returnTo
          delete req.session.returnTo
          res.redirect url
          return
        res.render 'user/authenticate'
    authenticator req, res, next

module.exports = UserController
