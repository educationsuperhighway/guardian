async             = require 'async'
bodyParser        = require 'body-parser'
cookieParser      = require 'cookie-parser'
cookieSession     = require 'cookie-session'
errorHandler      = require 'errorhandler'
express           = require 'express'
flash             = require 'connect-flash'
fml               = require 'form-model-validator'
fs                = require 'fs'
mailer            = require 'express-mailer'
mongoose          = require 'mongoose'
path              = require 'path'
stylus            = require 'stylus'
passport          = require 'passport'

config            = require './config'
logger            = require './logger'

app = express()
logger.configureExpress app
app.use cookieParser()
app.use bodyParser()
app.use cookieSession
  secret: 'brass monkey'
app.use passport.initialize()
app.use passport.session()
app.use flash()
app.set 'view engine', 'jade'
app.set 'views', path.join(__dirname, 'app', 'views')
app.get '*.min.css', (req, res, next) ->
  req.url = req.url + '.gz'
  res.set 'Content-Encoding', 'gzip'
  res.set 'Content-Type', 'text/css'
  next()
app.get '*.min.js', (req, res, next) ->
  req.url = req.url + '.gz'
  res.set 'Content-Encoding', 'gzip'
  res.set 'Content-Type', 'application/javascript'
  next()
app.use express.static(path.join(__dirname, 'public'))
app.use express.static(path.join(__dirname, 'vendor', 'assets'))
app.use errorHandler
  showStack: config.environment == 'development'
  dumpExceptions: config.environment == 'development'
fml.initialize()
mailer.extend app, config.mailer

app.models = []

fs.readdirSync(path.join(__dirname, 'app', 'models')).forEach (file) ->
  model = require path.join(__dirname, 'app', 'models', file)
  app.models.push(model)

require './auth'
require './oauth'

app.controllers = []

fs.readdirSync(path.join(__dirname, 'app', 'controllers')).forEach (file) ->
  Controller = require path.join(__dirname, 'app', 'controllers', file)
  app.controllers.push(new Controller(app))

mongoose.connect do () ->
  url = 'mongodb://'
  if config.db.user? && config.db.password?
    url += config.db.user + ':' + config.db.password + '@'
  url += config.db.host + ':' + config.db.port + '/' + config.db.name

app.listen 3000
